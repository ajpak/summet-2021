{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python Tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A few minutes ago, we discussed how to direct a mouse through a maze using \"pseudo\" code. Now let's implement these ideas using actual code. For this and the following exercises, we will be using Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python is a great general-purpose programming language on its own, but with the help of a few popular libraries (numpy, scipy, matplotlib), it becomes a powerful environment for scientific computing.\n",
    "\n",
    "We will first reinforce the programming concepts we just learned about (Booleans, loops, arrays, and functions) and introduce two useful packages: numpy and matplotlib\n",
    "\n",
    "This tutorial uses Python version 3 (version 2 is deprecated and no longer supported). If you're familiar with Python2, the most common difference is that the print statement in Python3 requires parentheses, as in `print(\"Hello world!\")`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This tutorial covers: \n",
    "\n",
    "* Basic Python: Conditionals, Numpy Arrays, and Loop Control\n",
    "* Numpy: Arrays, Array indexing, Array math\n",
    "* Matplotlib: Plotting, Subplots, Images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## What am I looking at right now?\n",
    "This is a \"Jupyter notebook\", a tool for using Python to interact directly with data and visualize it in a single place. If you've used Mathematica before, the idea is very similar. You hit `shift-enter` or `shift-return` to execute the code in a \"cell\". \n",
    "\n",
    "**Beware:**\n",
    "- the good thing about notebooks is that they let you interact with your data in very flexible ways\n",
    "- the bad thing is that you can execute cells out of order, and overwrite variables in ways you forget or didn't expect\n",
    "\n",
    "If you're getting weird results, it's best to either do `Cell->Run All` at the top to reset the entire notebook, or if really needed, `Kernel->Restart`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Basics of Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### Exercise 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's navigate our mouse from the \"Start\" to \"End\" position in the following maze:\n",
    "![Exercise 1](img/exercise1.png)\n",
    "\n",
    "We'll represent our current position as two *variables* = (*x*, *y*).\n",
    "X --> from left to right\n",
    "Y --> from bottom to top\n",
    "\n",
    "We'll adjust our variables by changing their value using Python.\n",
    "\n",
    "#### Some assumptions:\n",
    "\n",
    "\"Start\" = (1,4)\n",
    "\n",
    "\"End\" = (8,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### Numbers and operations in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integers and floats (or decimals) work as you would expect from other languages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "print( x )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(x + 1)  # Addition;\n",
    "print(x - 1)   # Subtraction;\n",
    "print(x * 2)  # Multiplication;\n",
    "print(x ** 2)  # Exponentiation;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# += and *= act on x and replace the value in it. \n",
    "# Try running this cell over and over again\n",
    "x += 1\n",
    "print(x)  # Prints \"4\", the first time\n",
    "x *= 2\n",
    "print(x) # Prints \"8\", the first time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that unlike many languages, Python does not have unary increment (x++) or decrement (x--) operators.\n",
    "\n",
    "Python also has built-in types for long integers and complex numbers; you can find all of the details in the [documentation](https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-long-complex)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### Coding directions\n",
    "To represent \"Left\", \"Right\", \"Up\", and \"Down\", we can add/subtract values to *x* and *y*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x=0\n",
    "y=0\n",
    "\n",
    "# Right (anything after a \"#\" is considered a comment in Python)\n",
    "x += 1\n",
    "\n",
    "# Left\n",
    "x -= 1\n",
    "\n",
    "# Up\n",
    "y += 1\n",
    "\n",
    "# Down\n",
    "y -= 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, please start from the \"Start\" position and find your way to the \"End\" position.\n",
    "\n",
    "In other words, we're going to initialize *x* = 1, *y* = 4 (Start) and perform operations until *x* = 8, *y* = 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x=1\n",
    "y=4\n",
    "print(\"Start Position: \"+str(x)+\",\"+str(y))\n",
    "\n",
    "# Insert the moves for Right, Right, Right, Down, Down, Right, Right, Right, Right\n",
    "# You'll want to replace the ??????\n",
    "????\n",
    "\n",
    "# Check that the End Position is 8,2\n",
    "print(\"End Position: \"+str(x)+\",\"+str(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### Exercise 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a new maze to navigate, where the use of *for loops* is beneficial:\n",
    "![Exercise 2](img/exercise2.png)\n",
    "\n",
    "We have the same moves as above (Left, Right, Up, Down), but now we will use loops."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### \"For\" Loops"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use \"for\" loops to specify how many times a block of code should be repeated. Let's demonstrate how a \"for\" loop could be used to calculate 7\\*6 by addition. The **range** function is a built-in Python function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mult=7*6\n",
    "sum=0\n",
    "# the asterisk below is needed to force python to print the values of the range iterator object\n",
    "print(*range(6))\n",
    "for i in range(6) :\n",
    "    sum+=7\n",
    "print(\"Sum is %d\" % sum)\n",
    "print(\"Multi is %d\" % mult)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### Numpy Arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numpy is the core library for scientific computing in Python. It provides a high-performance multidimensional array object, and tools for working with these arrays. If you are already familiar with MATLAB, you might find this tutorial useful to get started with Numpy.\n",
    "\n",
    "To use Numpy, we first need to import the numpy package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A numpy array is a grid of values, all of the same type, and is indexed by nonnegative integers. The first index in any direction always starts with **0**. The number of dimensions is the rank of the array; the shape of an array gives the size of the array along each dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.array([0,1,2,3,4,5])\n",
    "print (b)\n",
    "a = range(6)\n",
    "print(*a) # we use the * here to force the range object to show its values\n",
    "# the numpy array and the range list show similar information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to get a value from a specific point in the array, you can use indices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(b[0]) #first element (indices start from 0)\n",
    "print(b[2]) #third element\n",
    "print(b[-1]) #last element\n",
    "print(b[-2]) #second to last element"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numpy also provides many functions to create arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.zeros((4,2))  # Create an array of all zeros\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.full((3,2), 4.2) # Create a constant array\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Math operations can occur on **each** element of the array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array(range(3))\n",
    "print(a)\n",
    "a += 2\n",
    "print(a)\n",
    "a *= 10\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This brief overview has touched on many of the important things that you need to know about numpy, but is far from complete. Check out the [numpy reference](http://docs.scipy.org/doc/numpy/reference/) to find out much more about numpy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### \"For\" loops over arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can loop over the elements of an array like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numlist = np.random.random(3)\n",
    "for num in numlist:\n",
    "    print(num) #prints every element of numlist stored in the variable 'num'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want access to the index of each element within the body of a loop, use the built-in `enumerate` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, num in enumerate(numlist):\n",
    "    print( '#%d: %s' % (i + 1, num) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### Coding directions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember, to represent left/right/up/down, we add/subtract values to *x* and *y*. \n",
    "\n",
    "We want to go (Right+Up) three times then (Right+Right+Down+Down) two times.\n",
    "\n",
    "#### Assume:\n",
    "Start = (1,3)\n",
    "\n",
    "End = (8,2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x=1\n",
    "y=3\n",
    "print(\"Start Position: \"+str(x)+\",\"+str(y))\n",
    "\n",
    "# Insert the moves for (Right+Up)x3 and (Right, Right, Down, Down)x2\n",
    "# using for loops and ranges\n",
    "# You'll want to replace the ??????\n",
    "????\n",
    "\n",
    "# Check that the End Position is 8,2\n",
    "print(\"End Position: \"+str(x)+\",\"+str(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### Exercise 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final maze we will navigate is the one that uses Boolean decisions:\n",
    "![Exercise 3](img/exercise3.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Booleans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python implements all of the usual operators for Boolean logic, but uses English words rather than symbols (`&&`, `||`, etc.):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t, f = True, False\n",
    "print( type(t) ) # Prints \"<class 'bool'>\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we let's look at the operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print( t and f ) # Logical AND;\n",
    "print( t or f  ) # Logical OR;\n",
    "print( not t   ) # Logical NOT;\n",
    "print( t != f  ) # Logical XOR;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If/Else blocks provide two sets of code with either one executed depending on the condition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x=10 #try changing me\n",
    "if(x == 9) :\n",
    "    print(\"We checked x and it is equal to 9\")\n",
    "else :\n",
    "    print(\"We checked x and it is NOT equal to 9\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "#### Coding directions\n",
    "\n",
    "We will make a reference numpy array that contains the state of each *x*,*y* position using their indices\n",
    "\n",
    "If the value in the index (*x*,*y*) is 0, then that spot is open. Otherwise, it is occupied."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grid = np.ones((10,10))\n",
    "grid[1:7,6] = 0.0\n",
    "grid[3,3:7] = 0.0\n",
    "grid[3:9,3] = 0.0\n",
    "grid[6:9,5] = 0.0\n",
    "grid[8,4] = 0.0\n",
    "\n",
    "print(grid)\n",
    "# (1,6) is the start position; confirm that grid[1,6] = 0\n",
    "print(\"Grid state of start position is: \"+str(grid[1,6]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's traverse the upper path in the maze using the IF statement we discussed earlier.\n",
    "\n",
    "#### Assume:\n",
    "\n",
    "Start = (1,6)\n",
    "\n",
    "End = (8,3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#initialize start\n",
    "x=1\n",
    "y=6\n",
    "print(\"Start Position: \"+str(x)+\",\"+str(y))\n",
    "\n",
    "# Insert the IF/ELSE moves:\n",
    "# if (right space available) move right\n",
    "# else (move down)\n",
    "# the for loop is used as a counter to ensure 10 moves are made\n",
    "# You'll want to replace the ??????'s'\n",
    "for i in range(10) :\n",
    "    if(grid[???,???]==???) :\n",
    "            x+=1\n",
    "    else :\n",
    "            y-=1\n",
    "\n",
    "# Check that the End Position is 8,3\n",
    "print(\"End Position: \"+str(x)+\",\"+str(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matplotlib is a plotting library. This section gives a brief introduction to the `matplotlib.pyplot` module, which provides a plotting system similar to that of MATLAB."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By running this special iPython command, we will be displaying plots inline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most important function in `matplotlib` is plot, which allows you to plot 2D data. Here is a simple example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the x and y coordinates for points on a sine curve\n",
    "x = np.arange(0, 3 * np.pi, 0.1)\n",
    "y = np.sin(x)\n",
    "\n",
    "# Plot the points using matplotlib\n",
    "plt.plot(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With just a little bit of extra work we can easily plot multiple lines at once, and add a title, legend, and axis labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_sin = np.sin(x)\n",
    "y_cos = np.cos(x)\n",
    "\n",
    "# Plot the points using matplotlib\n",
    "plt.plot(x, y_sin)\n",
    "plt.plot(x, y_cos)\n",
    "plt.xlabel('x axis label')\n",
    "plt.ylabel('y axis label')\n",
    "plt.title('Sine and Cosine')\n",
    "plt.legend(['Sine', 'Cosine'])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
